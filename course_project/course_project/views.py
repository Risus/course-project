from flask import render_template, redirect, url_for
from flask.views import MethodView

from . import app
from .models import Product
from .models import User
from .models import Order
from .forms import UserForm
from .forms import ProductForm
from .forms import OrderForm


class HelloView(MethodView):
    def get(self):
        return render_template('main/hello.html')

app.add_url_rule('/', view_func=HelloView.as_view('main_hello'))


@app.route('/product', methods=['GET', 'POST'])
def order_add():
    products = Product.select().order_by(Product.id)
    form = OrderForm()
    if form.validate_on_submit():
        order = Order(
            firstname=form.firstname.data,
            lastname=form.lastname.data,
            product=form.product.data,
            value=form.value.data,
        )
        return redirect(url_for('order_add'))

    return render_template('product/list.html',
                           products=products, form=form,)


@app.route('/user/add', methods=['GET', 'POST'])
def user_add():
    form = UserForm()

    if form.validate_on_submit():
        user = User(
            username=form.username.data,
            password=form.password.data,
        )
        return redirect(url_for('user_add'))

    return render_template('user/add.html', form=form)


@app.route('/product/add', methods=['GET', 'POST'])
def product_add():
    form = ProductForm()

    if form.validate_on_submit():
        product = Product(
            product_code=form.product_code.data,
            title=form.title.data,
            description=form.description.data,
            price=form.price.data,
            value=form.value.data
        )
        return redirect(url_for('product_add'))

    return render_template('product/add.html', form=form)
