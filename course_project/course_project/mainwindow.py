import sys

from PyQt5.QtCore import QObject
from PyQt5.QtWidgets import (
    QApplication, QMainWindow, QWidget,
    QLabel, QDoubleSpinBox, QPushButton,
    QVBoxLayout, QMenuBar, QMenu,
    QTableView, QDesktopWidget, QHeaderView
)

from .addprofile import AdderProfile
from .addproduct import AdderProduct
from .delorder import DelerOrder
from .pooldb import DatabaseConn


class MainWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._db = DatabaseConn()
        self._initUi()
        self._initLayouts()
        self._initSignals()
        self._adderprof = None
        self._adderprod = None
        self._delerord = None

    def _initUi(self):
        self.winparam = QDesktopWidget().availableGeometry()
        self.wig = 700
        self.heig = 400
        self.setWindowTitle('Shop manager')
        self.setGeometry(int((self.winparam.width()-self.wig)/2),
                         int((self.winparam.height()-self.heig)/2),
                         self.wig, self.heig
                         )

        self.tblw = QWidget(self)
        self._prodTable = QTableView(self.tblw)
        tblwig = 600
        tblheig = 300
        self._prodTable.setGeometry((self.wig-tblwig)/2, (self.heig-tblheig)/2, tblwig, tblheig)
        self._prodTable.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self._prodTable.verticalHeader().setSectionResizeMode(QHeaderView.Stretch)

        self._menuList = QMenu('Menu', self)
        self._menuList.addAction('Add new profile...', self._onAddProfAction)
        self._menuList.addAction('Add new book...', self._onAddProdAction)
        self._menuList.addSeparator()
        self._menuList.addAction('Exit', self._onExitAction)

        self._showList = QMenu('Show', self)
        self._showList.addAction('Books', self._onShowBooksAction)
        self._showList.addAction('Orders', self._onShowOrdersAction)

        self._editList = QMenu('Edit', self)
        self._editList.addAction('Del order by id..', self._onDelOrderAction)

        self._menuBar = QMenuBar(self)
        self._menuBar.resize(self.wig, 30)
        self._menuBar.addMenu(self._menuList)
        self._menuBar.addSeparator()
        self._menuBar.addMenu(self._showList)
        self._menuBar.addMenu(self._editList)

    def _initLayouts(self):
        self.setCentralWidget(self.tblw)

    def _initSignals(self):
        pass

    def _onAddProfAction(self):
        if not self._adderprof:
            self._adderprof = AdderProfile(self)
        self._adderprof.show()

    def _onAddProdAction(self):
        if not self._adderprod:
            self._adderprod = AdderProduct(self)
        self._adderprod.show()

    def _onShowBooksAction(self):
        self._prodTable.setModel(self._db.getProductTable())

    def _onShowOrdersAction(self):
        self._prodTable.setModel(self._db.getOrdersTable())

    def _onDelOrderAction(self):
        if not self._delerord:
            self._delerord = DelerOrder(self)
        self._delerord.show()

    def _onExitAction(self):
        self.close()
