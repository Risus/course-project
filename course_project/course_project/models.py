from pony.orm import (
    Database,
    Required, Optional, PrimaryKey, Set
)


db = Database()


class Product(db.Entity):
    """Товар"""
    product_code = Required(str, 20)
    title = Required(str, 30)
    description = Required(str, 200)
    price = Required(float)
    value = Required(int)


class User(db.Entity):
    """Админы"""
    username = Required(str, 20)
    password = Required(str, 500)


class Order(db.Entity):
    """Заказ"""
    firstname = Required(str, 20)
    lastname = Required(str, 20)
    product = Required(str, 20)
    value = Required(int)
