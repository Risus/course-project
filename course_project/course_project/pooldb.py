from PyQt5.QtCore import QObject
from PyQt5.QtSql import (
    QSqlDatabase, QSqlQuery, QSqlQueryModel
)


class DatabaseConn(QObject):
    def _conn(self):
        self.db = QSqlDatabase.addDatabase('QSQLITE')
        self.db.setDatabaseName('course_project/simplyshop.sqlite')

        if not self.db.open():
            raise Error("Could not open the database")
        print('connection success!')

    def getUserPass(self, username):
        self._conn()
        query = QSqlQuery(self.db)
        request = "SELECT username, password FROM User"
        query.exec_(request)
        pas = None

        while query.next():
            if username == query.value(0):
                pas = query.value(1)
                break

        self.db.close()

        return pas

    def getProductTable(self):
        self._conn()
        model = QSqlQueryModel(self)
        model.setQuery("""
            SELECT
                'Product'.product_code AS Code,
                'Product'.title AS Title,
                'Product'.description AS Description,
                'Product'.price AS Price,
                'Product'.value AS Value
            FROM
                'Product'
        """)
        self.db.close()

        return model

    def getOrdersTable(self):
        self._conn()
        model = QSqlQueryModel(self)
        model.setQuery("""
            SELECT
                'Order'.id AS ID,
                'Order'.firstname AS Firstname,
                'Order'.lastname AS Lastname,
                'Order'.product AS Product,
                'Order'.value AS Value
            FROM
                'Order'
        """)
        self.db.close()

        return model

    def addProduct(self, argw):
        self._conn()
        query = QSqlQuery(self.db)
        request = """
            INSERT INTO Product
                (product_code, title, description, price, value)
            VALUES
                (?, ?, ?, ?, ?)
        """
        query.prepare(request)
        i = 0

        while i < 5:
            query.bindValue(i, argw[i])
            i += 1

        query.exec_()
        self.db.close()

    def addProfile(self, argw):
        self._conn()
        query = QSqlQuery(self.db)
        request = """
            INSERT INTO User
                (username, password)
            VALUES
                (?, ?)
        """
        query.prepare(request)
        i = 0

        while i < 2:
            query.bindValue(i, argw[i])
            i += 1

        query.exec_()
        self.db.close()

    def delOrder(self, id):
        self._conn()
        query = QSqlQuery(self.db)
        request = """DELETE FROM 'Order' WHERE 'Order'.id=""" + id
        query.exec_(request)
        self.db.close()
