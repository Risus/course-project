from flask_wtf import FlaskForm
from wtforms.fields import(
    StringField, IntegerField, FloatField
)
from wtforms.validators import (
    InputRequired,
    Length,
    NumberRange 
)


class UserForm(FlaskForm):
    username = StringField('Пользователь', validators=[
    InputRequired(), Length(min=5, max=20)
    ])
    password = StringField('Пароль', validators=[
    InputRequired(), Length(min=4, max=500)
    ])


class OrderForm(FlaskForm):
    firstname = StringField('Имя', validators=[
        InputRequired(), Length(min=2, max=20)
    ])
    lastname = StringField('Фамилия', validators=[
        InputRequired(), Length(min=2, max=20)
    ])
    product = StringField('Код продукта', validators=[
        InputRequired(), Length(min=5, max=20)
    ])
    value = IntegerField('Колличество', validators=[
        InputRequired()
    ])


class ProductForm(FlaskForm):
    product_code = StringField('Код продукта', validators=[
        InputRequired(), Length(min=5, max=20)
    ])
    title = StringField('Название', validators=[
        InputRequired(), Length(max=30)
    ])
    description = StringField('Описание', validators=[
        InputRequired(), Length(max=200)
    ])
    price = FloatField('Стоимость', validators=[
        InputRequired(), NumberRange(min=1)
    ])
    value = IntegerField('Колличество', validators=[
        InputRequired()
    ])
