import sys
import hashlib

from PyQt5.QtCore import QObject
from PyQt5.QtWidgets import (
    QApplication, QDialog, QWidget,
    QLabel, QLineEdit, QPushButton,
    QVBoxLayout, QHBoxLayout
)

from .pooldb import DatabaseConn


class AdderProfile(QDialog):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._initUi()
        self._initLayouts()
        self._initSignals()

    def _initUi(self):
        self.setWindowTitle('Add profile')

        self._usernameLabel = QLabel('Username:', self)
        self._passwordLabel = QLabel('Password:', self)
        self._repasswordLabel = QLabel('Repeat password:', self)
        self._messageLabel = QLabel('', self)

        self._usernameEdit = QLineEdit(self)
        self._usernameEdit.setMaxLength(20)

        self._passwordEdit = QLineEdit(self)
        self._passwordEdit.setMaxLength(16)
        self._passwordEdit.setEchoMode(QLineEdit.Password)

        self._repasswordEdit = QLineEdit(self)
        self._repasswordEdit.setMaxLength(16)
        self._repasswordEdit.setEchoMode(QLineEdit.Password)

        self._AddBtn = QPushButton('Add', self)
        self._CancelBtn = QPushButton('Cancel', self)

    def _initLayouts(self):
        subwidget = QWidget(self)

        self._mainLayout = QVBoxLayout(self)
        self._subLayout = QHBoxLayout(subwidget)

        self._mainLayout.addWidget(self._usernameLabel)
        self._mainLayout.addWidget(self._usernameEdit)
        self._mainLayout.addWidget(self._passwordLabel)
        self._mainLayout.addWidget(self._passwordEdit)
        self._mainLayout.addWidget(self._repasswordLabel)
        self._mainLayout.addWidget(self._repasswordEdit)
        self._mainLayout.addWidget(self._messageLabel)

        self._subLayout.addWidget(self._AddBtn)
        self._subLayout.addWidget(self._CancelBtn)
        self._mainLayout.addWidget(subwidget)

    def _initSignals(self):
        self._AddBtn.clicked.connect(self._onAddBtn)
        self._CancelBtn.clicked.connect(self._onCancelBtn)

    def _onAddBtn(self):
        profhandler = ProfHandler(self._usernameEdit.text(),
                                  self._passwordEdit.text(),
                                  self._repasswordEdit.text(),
                                  self
                                  )

        if not profhandler.addProf():
            self._messageLabel.setText('Invalid password')
        else:
            self.close()

    def _onCancelBtn(self):
        self.close()


class ProfHandler(QObject):
    def __init__(self, username, password, repassword, parent=None):
        super().__init__(parent)
        self._username = username
        self._password = password
        self._repassword = repassword

    def _validatePass(self):
        return self._password == self._repassword

    def addProf(self):
        if not self._validatePass():
            return False

        pw = hashlib.md5(self._password.encode()).hexdigest()
        DatabaseConn().addProfile((self._username, pw))
        return True
