import sys
import hashlib

from PyQt5.QtCore import QObject
from PyQt5.QtWidgets import (
    QApplication, QDialog, QWidget,
    QLabel, QLineEdit, QPushButton,
    QVBoxLayout, QHBoxLayout
)

from .mainwindow import MainWindow
from .pooldb import DatabaseConn


class AuthDialog(QDialog):
    def __init__(self, main_w, *args, **kwargs):
        super().__init__(main_w, *args, **kwargs)
        self._mainW = main_w
        self._initUi()
        self._initLayouts()
        self._initSignals()

    def _initUi(self):
        self.setWindowTitle('Authentication')

        self._usernameLabel = QLabel('Username:', self)
        self._passwordLabel = QLabel('Password:', self)
        self._messageLabel = QLabel('', self)

        self._usernameEdit = QLineEdit(self)
        self._usernameEdit.setMaxLength(20)

        self._passwordEdit = QLineEdit(self)
        self._passwordEdit.setMaxLength(16)
        self._passwordEdit.setEchoMode(QLineEdit.Password)

        self._OKBtn = QPushButton('OK', self)
        self._ExitBtn = QPushButton('Exit', self)

    def _initLayouts(self):
        subwidget = QWidget(self)

        self._mainLayout = QVBoxLayout(self)
        self._subLayout = QHBoxLayout(subwidget)

        self._mainLayout.addWidget(self._usernameLabel)
        self._mainLayout.addWidget(self._usernameEdit)
        self._mainLayout.addWidget(self._passwordLabel)
        self._mainLayout.addWidget(self._passwordEdit)
        self._mainLayout.addWidget(self._messageLabel)

        self._subLayout.addWidget(self._OKBtn)
        self._subLayout.addWidget(self._ExitBtn)
        self._mainLayout.addWidget(subwidget)


    def _initSignals(self):
        self._OKBtn.clicked.connect(self._onOKBtn)
        self._ExitBtn.clicked.connect(self._onExitBtn)

    def _onOKBtn(self):
        authhandler = AuthHandler(self._usernameEdit.text(),
                                  self._passwordEdit.text(),
                                  self
                                  )

        # self._mainW.show()
        # self.close()
        if authhandler.checkUser():
            self._mainW.show()
            self.close()

        self._messageLabel.setText('Invalid username or password')

    def _onExitBtn(self):
        self.close()


class AuthHandler(QObject):
    def __init__(self, username, password, parent=None):
        super().__init__(parent)
        self._username = username
        self._password = password

    def checkUser(self):
        pw = hashlib.md5(self._password.encode()).hexdigest()
        dbpw = DatabaseConn().getUserPass(self._username)
        return pw == dbpw


if __name__ == '__main__':
    app = QApplication(sys.argv)

    w = AuthDialog()
    w.show()

    sys.exit(app.exec_())
