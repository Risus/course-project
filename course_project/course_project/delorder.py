import sys

from PyQt5.QtCore import QObject
from PyQt5.QtWidgets import (
    QApplication, QDialog, QWidget,
    QLabel, QSpinBox, QPushButton,
    QVBoxLayout, QHBoxLayout
)

from .pooldb import DatabaseConn


class DelerOrder(QDialog):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._initUi()
        self._initLayouts()
        self._initSignals()

    def _initUi(self):
        self.setWindowTitle('Delete order by ID')

        self._idLabel = QLabel('Enter ID:', self)

        self._idEdit = QSpinBox(self)
        self._idEdit.setRange(1, 999999)

        self._DelBtn = QPushButton('Delete', self)
        self._CancelBtn = QPushButton('Cancel', self)

    def _initLayouts(self):
        subwidget = QWidget(self)

        self._mainLayout = QVBoxLayout(self)
        self._subLayout = QHBoxLayout(subwidget)

        self._mainLayout.addWidget(self._idLabel)
        self._mainLayout.addWidget(self._idEdit)

        self._subLayout.addWidget(self._DelBtn)
        self._subLayout.addWidget(self._CancelBtn)
        self._mainLayout.addWidget(subwidget)

    def _initSignals(self):
        self._DelBtn.clicked.connect(self._onDelBtn)
        self._CancelBtn.clicked.connect(self._onCancelBtn)

    def _onDelBtn(self):
        DatabaseConn().delOrder(self._idEdit.text())
        self.close()

    def _onCancelBtn(self):
        self.close()
