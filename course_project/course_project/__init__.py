from flask import Flask
from pony.flask import Pony
from flask_wtf import CSRFProtect
from flask_bootstrap import Bootstrap


app = Flask(__name__)
app.config.update({
    'SECRET_KEY': 'Sth',
    'PONY': {
        'provider': 'sqlite',
        'filename': 'simplyshop.sqlite',
        'create_db': True
    }
})


Pony(app)
CSRFProtect(app)
Bootstrap(app)


from . import models, views

models.db.bind(**app.config.get('PONY'))
models.db.generate_mapping(create_tables=True)
