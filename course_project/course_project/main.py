import sys

from PyQt5.QtWidgets import QApplication

from .authentication import AuthDialog
from .mainwindow import MainWindow


def main():
    app = QApplication(sys.argv)

    main_w = MainWindow()
    auth_w = AuthDialog(main_w)

    auth_w.show()

    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
