import sys

from PyQt5.QtCore import QObject
from PyQt5.QtWidgets import (
    QApplication, QDialog, QWidget,
    QLabel, QLineEdit, QPushButton,
    QDoubleSpinBox, QVBoxLayout, QHBoxLayout,
    QSpinBox
)

from .pooldb import DatabaseConn


class AdderProduct(QDialog):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._initUi()
        self._initLayouts()
        self._initSignals()

    def _initUi(self):
        self.setWindowTitle('Add book')

        self._productcodeLabel = QLabel('Code:', self)
        self._titleLabel = QLabel('Title:', self)
        self._descriptionLabel = QLabel('Description:', self)
        self._priceLabel = QLabel('Price:', self)
        self._valueLabel = QLabel('Value:', self)

        self._productcodeEdit = QLineEdit(self)
        self._productcodeEdit.setMaxLength(20)

        self._titleEdit = QLineEdit(self)
        self._titleEdit.setMaxLength(30)

        self._descriptionEdit = QLineEdit(self)
        self._descriptionEdit.setMaxLength(200)

        self._priceEdit = QDoubleSpinBox(self)
        self._priceEdit.setRange(1, 999999999)

        self._valueEdit = QSpinBox(self)
        self._valueEdit.setRange(0, 999999)

        self._AddBtn = QPushButton('Add', self)
        self._CancelBtn = QPushButton('Cancel', self)

    def _initLayouts(self):
        subwidget = QWidget(self)

        self._mainLayout = QVBoxLayout(self)
        self._subLayout = QHBoxLayout(subwidget)

        self._mainLayout.addWidget(self._productcodeLabel)
        self._mainLayout.addWidget(self._productcodeEdit)
        self._mainLayout.addWidget(self._titleLabel)
        self._mainLayout.addWidget(self._titleEdit)
        self._mainLayout.addWidget(self._descriptionLabel)
        self._mainLayout.addWidget(self._descriptionEdit)
        self._mainLayout.addWidget(self._priceLabel)
        self._mainLayout.addWidget(self._priceEdit)
        self._mainLayout.addWidget(self._valueLabel)
        self._mainLayout.addWidget(self._valueEdit)

        self._subLayout.addWidget(self._AddBtn)
        self._subLayout.addWidget(self._CancelBtn)
        self._mainLayout.addWidget(subwidget)

    def _initSignals(self):
        self._AddBtn.clicked.connect(self._onAddBtn)
        self._CancelBtn.clicked.connect(self._onCancelBtn)

    def _onAddBtn(self):
        argw = (self._productcodeEdit.text(),
                self._titleEdit.text(),
                self._descriptionEdit.text(),
                self._priceEdit.value(),
                self._valueEdit.value())

        DatabaseConn().addProduct(argw)
        self.close()

    def _onCancelBtn(self):
        self.close()
