from setuptools import setup


setup(
    name='shop_manager',
    version='0.0.1',
    py_modules=['shop_manager'],
    description='Communicates with server database using GUI.',
    autor='Nikita [Rizus]',
    autor_email='max_950@mail.ru',
    packages=['course_project'],
    install_requires=[
        'flask',
        'flask-wtf',
        'flask-bootstrap',
        'pony',
        'pyqt5'
    ]
)
